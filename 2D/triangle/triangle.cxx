#encoding "utf8"

Name -> Word<kwtype=name>;

Group -> (Word<kwset=["triangleType","baseType"]> interp(Entity2D.Type)) Word<kwtype=basicTriangle, rt> interp(Entity2D.Figure);
Group -> (Word<kwset=["trapezeType","baseType"]> interp(Entity2D.Type)) Word<kwtype=basicTrapeze, rt> interp(Entity2D.Figure);
Group -> (Word<kwset=["polygonType","baseType"]> interp(Entity2D.Type)) Word<kwtype=basicPolygon> interp(Entity2D.Figure);
Group -> (Word<kwtype=baseType> interp(Entity2D.Type)) Word<kwset=["basicQuadrangle","basicCircle"]> interp(Entity2D.Figure);

S -> Group (Name interp(Entity2D.Name));