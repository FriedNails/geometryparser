#encoding "utf8"

Type -> Word<kwtype=angleType>;
Angle -> Word<kwtype=basicAngle, rt>;
Name -> Word<kwtype=name>;

S -> (Type interp (Angle.Type)) Angle interp (Angle.Figure) (Name interp(Angle.Name));
S -> (Name interp(Angle.Name)) Angle interp (Angle.Figure) (Type interp (Angle.Type));