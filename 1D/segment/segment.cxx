#encoding "utf8"

// Name -> Word<kwtype=name>;
// Seg -> Word<kwset=["segment","chord","radius","diameter","altitude","median","bisector"], rt>;
// Act -> Verb;

// S -> Seg interp(Segment.Figure) (Name interp(Segment.Name)) (Act interp(Segment.Action));
// S -> (Name interp(Segment.Name)) Seg interp(Segment.Figure);

Name -> Word<kwtype=name> interp(Segment.Name) ;
Seg -> Word<kwset=["segment","chord","radius","diameter","altitude","median","bisector","basicTangent"], rt> interp(Segment.Figure);
Act -> Verb interp(Segment.Action);

S -> Seg (Name) (Act);
S -> (Name) Seg;